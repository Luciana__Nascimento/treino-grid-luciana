# Treino-grid-Luciana

Segue repositório com arquivos css grid


# Daqui pra baixo treino markdown

Este é um README para treinar o Markdown


## Getting started

_itálico em markdown_

**negrito em markdown**

Links inline: [exemplo de link para o github](https://github.com/)

imagens: [imagem acct](https://nerdin.com.br/img/empresa/1176.png)

imagens: ![download](/uploads/a8e5348af776948b2fbcef8163a5df22/download.jpg)

Tabela |Treino | Estágio|
:----:| :----: | :----:|
Tabela |Treino | Estágio|
Tabela |Treino | Estágio|


Bloco de código:
```json
{
  "firstName": "Luciana",
  "lastName": "Nascimento",
  "age": 31
}
```

Nota de rodapé: 
exemplo de link para a nota de rodapé no markdown [^1]


[^1]: exemplo de nota de rodapé no markdown


Títulos no Markdown:

# Título 1
## Título 2
### Título 3
#### Título 4
##### Título 5
###### Título 6



Tachado:

Luciana  ~~Luciana~~



Lista de tarefas:

- [x] tarefa 1
- [ ] tarefa 2
- [ ] tarefa 3


Emoji:

:dog2: :heart:



Realçar:

==Luciana==


Subscrito:

Luciana ~N~ Gonçalves



vinculação automatica de URL:

https://www.google.com/


Desativando vinculação automatica de URL:

`https://www.google.com/`


Lista Ordenada:

1. Primeiro item
2. Segundo item
3. Terceiro item

Lista não Ordenada:

- Primeiro item
- Segundo item
- Terceiro item

